# Functional programming

Vesta is intended to be functional in many ways. Lists are immutable, lists
can work as first-class functions, words cannot be redefined, etc. In this
sense, every call to a word is like a function call, that updates the
stack(s).

However, in certain settings, it may be better to be more like Forth. E.g. in
interactive mode, maybe we can add ways to "forget" words or redefine them.
There should also be words to save and restore the "world" from images.

