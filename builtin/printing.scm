;; printing.scm

;; ? x --
;; Print a literal representation of x, followed by a newline.
;; XXX should use [ ] for lists, not ( )
(register '?
          (lambda (state)
            (let ((x (pop-data! state)))
              (printf "~a~%" x))))

(define *character-names*
  '((space        " ")
    (newline      "\n")
    (double-quote "\"")
    (single-quote "'")
    (apostrophe   "'")
    (left-brace   "[")
    (right-brace  "]")
    (tab          "\t")))

(define (print-symbol-as-char sym)
  (let ((result (assoc sym *character-names*)))
    (if result
        (printf "~a" (cadr result))
        (error "Unprintable character:" sym))))

(define (print-char x)
  (cond
   ((number? x)
    (printf "~a" (integer->char x)))
   ((symbol? x)
    (print-symbol-as-char x))
   ((list? x)
    (for-each print-char x))))

(register '?c
          (lambda (state)
            (let ((x (pop-data! state)))
              (print-char x))))
