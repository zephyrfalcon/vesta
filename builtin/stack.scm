;; stack.scm
;; Common stack manipulation words.

(register 'dup
          (lambda (state)
            (let ((x (pop-data! state)))
              (push-many! state x x))))

(register 'drop
          (lambda (state)
            (pop-data! state)))

(register 'swap
          (lambda (state)
            (receive (a b) (pop-many! state 2)
              (push-many! state b a))))

(register 'over
          (lambda (state)
            (receive (a b) (pop-many! state 2)
              (push-many! state a b a))))
