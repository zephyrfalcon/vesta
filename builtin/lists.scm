;; lists.scm

(register 'cons
          (lambda (state)
            (receive (x lst) (pop-many! state 2)
              (assert (list? lst))
              (push-data! state (cons x lst)))))

(register 'uncons
          (lambda (state)
            (let ((lst (pop-data! state)))
              (push-many! state (car lst) (cdr lst)))))

(register 'length
          (lambda (state)
            (let ((lst (pop-data! state)))
              (push-data! state (length lst)))))

(register 'head
          (lambda (state)
            (let ((lst (pop-data! state)))
              (push-data! state (car lst)))))

(register 'tail
          (lambda (state)
            (let ((lst (pop-data! state)))
              (push-data! state (cdr lst)))))
