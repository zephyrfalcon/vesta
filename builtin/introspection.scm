;; introspection.scm

(define (get-all-words ns)
  (let ((local-words (namespace-keys ns)))
    (if (namespace-parent ns)
        (append local-words (get-all-words (namespace-parent ns)))
        local-words)))

(register 'words
          (lambda (state)
            (let ((words (namespace-all-keys (current-namespace state))))
              (push-data! state words))))
