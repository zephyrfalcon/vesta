;; recursion.scm

#| tailrec:
We would need:
1. some initial value? maybe not
2. code that processes the data
3. code that checks what to do next (i.e. done, or recurse)
4. code that goes on the code stack next

6 fac
code is really: *
start with 1 and the number given; do *; push N-1; do *, etc
until 1 is reached, then we're done

[fac [n]
     1
     [*]
     [1 -]   \ dec for counter
     [...counter = 1?]
     tailrec
].

|#

