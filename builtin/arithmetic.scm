;; arithmetic.scm

(define *binops*
  `((+  ,+)
    (-  ,-)
    (*  ,*)
    (/  ,quotient)
    (// ,remainder)))

;; register binops
(for-each (lambda (z)
            (let ((name (first z))
                  (f (second z)))
              (register name
                        (lambda (state)
                          (let ((b (pop-data! state))
                                (a (pop-data! state)))
                            (push-data! state (f a b)))))))
          *binops*)

(register 'neg
          (lambda (state)
            (let ((x (pop-data! state)))
              (push-data! state (- x)))))

