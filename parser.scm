;; parser.scm

(define (token->obj token)
  (case (car token)
    ((number) (string->number (second token)))
    ((symbol) (string->symbol (second token)))
    (else (error 'token->obj "Invalid token"))))

(define (parse-tokens tokens)
  (let loop ((tokens tokens)
             (code-stack '(())))
    (cond
     ((null? tokens) ;; done
      (cond
       ((> (length code-stack) 1) (error "Unterminated list"))
       ((< (length code-stack) 1) (error "Too many closing brackets"))
       (else (rec-reverse (car code-stack)))))
     
     ((eq? 'lbrace (caar tokens))
      ;; push a new list onto the code stack
      (loop (cdr tokens) (cons '() code-stack)))
     
     ((eq? 'rbrace (caar tokens))
      ;; pop the topmost list off of the code stack, then cons it onto the
      ;; next topmost list
      ;; [[a b c] [c d e] ...] => [[[a b c] c d e] ...]
      (let* ((top-of-stack (car code-stack))
             (below-top (second code-stack))
             (rest-of-stack (cddr code-stack))) ; may be empty
       (loop (cdr tokens)
             (cons (cons top-of-stack below-top) rest-of-stack))))

     ;; atoms are consed onto the topmost list of the code stack
     (else (loop (cdr tokens)
                 (cons (cons (token->obj (car tokens)) (car code-stack))
                       (cdr code-stack)))))))

(define (parse data)
  (parse-tokens (tokenize data)))
