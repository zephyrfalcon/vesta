;; test-interpreter.scm

(use test)
(load "vesta")

(define (evaluate line)
  (let* ((code (parse line))
         (state (make-initial-state code)))
    (execute-code state)
    (state-data-stack state)))

(test-group
 "arithmetic"

 (test '(3) (evaluate "1 2 +"))
 )

(test-group
 "definitions"

 (test '(5) (evaluate "[inc 1 +]. 4 inc"))
 (test '(42) (evaluate "[magic 42]. magic"))
 (test '(8) (evaluate "[twice [a] a a +]. 4 twice")) ;; with arguments
 )

(test-group
 "conditionals"

 (test '(2) (evaluate "0 1 2 ifte"))
 (test '(1) (evaluate "1 1 2 ifte"))
 (test '(3) (evaluate "1 [1 2 +] [3 4 +] ifte"))
 )