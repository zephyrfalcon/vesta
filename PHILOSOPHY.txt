# Vesta philosophy

- stack-based
- functional
- READABLE (ideas taken from V)
- only 3 data types: numbers, symbols, lists
  benefits:
  - easy serialization
  - easy printing :)
  - easy storing in Scheme
  - use lists to build abstractions
  - etc...
- dynamic scoping
- concise
- context-dependent:
  what something means depends on what a word does with it
- lists are immutable!
- definitions are immutable! (i.e. cannot be redefined)

- values go on the stack. words go in an implicit namespace (implicit as in,
  we cannot directly access it like in Delta). words are executable. they
  always refer to "functions" (i.e. lists in a specific format), never to
  values. although they COULD, I suppose.

- defined values cannot be changed. (functional; compare e.g. OCaml)
  (unless in interactive mode?)

- most of all, the language should be SIMPLE... simple structures, simple
  words, encourage values to appear close to the words that use them.

- what it DOES NOT have:
  - floats (but we can simulate them)
  - ditto for complex, ratios
  - strings!
  - Unicode
  - dictionaries :-/
  - structs and objects (again, can be simulated)
  - function objects! (we store lists, what happens with them depends on
    context; no associated namespace; no closures)
  - file objects

