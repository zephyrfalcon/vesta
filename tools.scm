;; tools.scm

(define (rec-reverse lst)
  (reverse (map (lambda (x) (if (list? x) (rec-reverse x) x))
                lst)))

;; make sure that X is a list. if it's an atom, wrap it in a list.
(define (force-list x)
  (if (list? x)
      x
      (list x)))

;; take items from LST while PRED is true for those items.
;; returns two values, one of the items taken, one of the items left.
(define (split-while pred lst)
  (define (split-while-aux taken left)
    (if (or (null? left)
            (not (pred (car left))))
        (values (reverse taken) left)
        (split-while-aux (cons (car left) taken) (cdr left))))
  (split-while-aux '() lst))

(define (unique lst member?)
  (define (unique-aux acc rest)
    (if (null? rest)
        (reverse acc)
        (if (member? (car rest) acc)
            (unique-aux acc (cdr rest))
            (unique-aux (cons (car rest) acc) (cdr rest)))))
  (unique-aux '() lst))

(define (read-file-contents filename)
  ;; Return file contents as one large string.
  (with-input-from-file filename
    read-string))
