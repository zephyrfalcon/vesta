;; test-tools.scm

(use test)
(load "tools")

(test-group
 "rec-reverse"

 (test '(3 2 1) (rec-reverse '(1 2 3)))
 (test '(5 (4 3) 2 1) (rec-reverse '(1 2 (3 4) 5)))
 )
