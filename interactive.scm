;; interactive.scm

(define *prompt* ">> ")
(define *welcome-msg* "Welcome to Vesta 0.00001.")

(define (load-stdlib state)
  ;;(print "Loading stdlib...")
  (let* ((src (read-file-contents "std.vs"))
         (code (parse src)))
    ;;(print code)
    (push-code! state code)
    (execute-code state)))

(define (interactive-loop state)
  (print *welcome-msg*)
  (let loop ((state state))
    (printf *prompt*)
    (let ((line (read-line)))
      (if (eq? line #!eof)
          (print) ;; leave loop
          (begin
            (execute-line state line)
            (print-stack state)
            (loop state))))))

(define (execute-line state line)
  (let ((code (parse line)))
    (push-code! state code)
    (execute-code state)))

(define (print-stack state)
  (let ((ds (state-data-stack state)))
    (printf "#~a: ~a~%" (length ds) (reverse ds))))
