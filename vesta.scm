;; vesta.scm
;; With arguments, executes the specified files.
;; Without arguments, goes into interactive mode.

(load "tools")
(load "tokenizer")
(load "parser")
(load "namespace")
(load "state")
(load "builtins")
(load-relative "builtin/arithmetic")
(load-relative "builtin/stack")
(load-relative "builtin/introspection")
(load-relative "builtin/printing")
(load-relative "builtin/lists")
(load "interactive")

(define (main args)
  (let ((st (make-initial-state '())))
    (load-stdlib st)
    (if (null? args)
        (interactive-loop st)
        (error "not implemented yet"))))
