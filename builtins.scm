;; builtins.scm

;; this namespace is immutable and can be used by several interpreter states
;; at once, if necessary.
(define *builtin-namespace*
  (make-namespace #f (make-hash-table #:size 100)))

(define (register symbol f)
  (let ((hash (namespace-names *builtin-namespace*)))
    (hash-table-set! hash symbol f)))

;; ---
;; other built-in words are in the builtin/ directory.

(register '|.|
          (lambda (state)
            (let ((lst (pop-data! state)))
              (assert (list? lst) "word definition must be a list")
              (assert (symbol? (car lst)) "word name must be a symbol")
              ;; if there is no arglist, add one (empty)
              (when (and (> (length lst) 1)
                         (not (list? (second lst))))
                (set! lst (cons (car lst) (cons '() (cdr lst)))))
              (let ((ns (current-namespace state)))
                (namespace-define! ns (car lst) (cdr lst))))))

(register '|[popns]|  ; not accessible by user due to name
          pop-namespace!)

(register 'ifte
          (lambda (state)
            (receive (cnd then else)
                (pop-many! state 3)
              (if (equal? cnd 0)
                  (execute-block state else)
                  (execute-block state then)))))

;; evaluate a list of words
(register 'eval
          (lambda (state)
            (let ((code (pop-data! state)))
              (execute-block state code))))

;; isn't 'dip' just: pop x, eval, push x back? ja zeker...

(register 'let.
          (lambda (state)
            (let ((names (pop-data! state)))
              (assert (list? names))
              (bind-arguments state (reverse names)
                              (current-namespace state)))))

(register 'llet.
          (lambda (state)
            (let ((names (pop-data! state))
                  (vals (pop-data! state)))
              (for-each (lambda (name val)
                          (push-data! state val)
                          (bind-arguments state (list name)
                                          (current-namespace state)))
                        names vals))))
