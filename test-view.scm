;; test-view.scm

(use test)
(load "tools")
(load "view")

;; to get around annoying limitation of TEST
(define (id x) x)

(test-group
 "split-view-list"

 (receive (before after)
     (split-view-list '(a b : a b a))
   (test '(a b) (id before))
   (test '(a b a) (id after)))

  (receive (before after)
     (split-view-list '(a b :))
   (test '(a b) (id before))
   (test '() (id after)))
 )

 