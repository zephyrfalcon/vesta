;; test-parser.scm

(use test)
(load "tokenizer")
(load "tools")
(load "parser")

(test-group
 "token->obj"

 (test 3 (token->obj '(number "3")))
 (test 'foo (token->obj '(symbol "foo")))
 )

(test-group
 "parser"

 (test '(1) (parse "1"))
 (test '(1 2 3) (parse "1 2 3"))
 (test '(2 dup +) (parse "2 dup +"))

 (test '((trip (a) a a) |.|) (parse "[trip [a] a a]."))
 (test '(() dup) (parse "[] dup"))
 (test '(1 2 3) (parse "\\ bogus\n1\n2\n3"))

 (test '() (parse " "))
 )
