\ quadratic-root.vs

[quadratic-root [a b c]
    [minisub 0 b -].
    [radical b b * 4 a * c * - sqrt].   \ we don't have sqrt yet :-/
    [divisor 2 a *].
    [root1 minisub radical + divisor /].
    [root2 minisub radical - divisor /].
    root1 root2
].

