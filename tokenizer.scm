;; tokenizer.scm

(use regex)
(use srfi-1)
(use srfi-13)

(define number-regex (regexp "^-?\\d+"))
(define symbol-regex (regexp "^[a-zA-Z0-9_<>=!?/.,:;&^*+-]+"))
(define lbrace-regex (regexp "^\\["))
(define rbrace-regex (regexp "^\\]"))
(define whitespace-regex (regexp "^[ \\\t\\\r\\\n]+"))
;;(define comment-regex (regexp "^\\\\.*?\n"))
(define comment-regex (regexp '(: "\\" (*? any) "\n")))

(define *token-regexen*
  (list (list number-regex 'number)
        (list symbol-regex 'symbol)
        (list lbrace-regex 'lbrace)
        (list rbrace-regex 'rbrace)
        (list whitespace-regex 'whitespace)
        (list comment-regex 'comment)))

(define (match-regex regex data)
  (and-let* ((m (string-search regex data)))
            (car m)))

(define (find-match data)
  (let loop ((regexen *token-regexen*))
    (if (null? regexen)
        (error 'find-match "Syntax error")
        (let* ((match (match-regex (first (car regexen)) data)))
          (if match
              (list (second (car regexen)) match)
              (loop (cdr regexen)))))))

(define (tokenize data)
  (let loop ((data data)
             (tokens '()))
    
    ;; if data is empty, we're done
    (if (= (string-length data) 0)
        (reverse tokens)
        ;; otherwise, find a match, store or discard, and move on
        (let* ((m (find-match data))
               (rest (substring data (string-length (second m)))))
          (case (first m)
            ((comment whitespace) (loop rest tokens))
            (else (loop rest (cons m tokens))))))))
