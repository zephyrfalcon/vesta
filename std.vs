\ simple standard library functions

\\ stack manipulation
[dip [x code] code eval x].

\\ lists
[unit [] [] cons]. \ create a single-element list
[unquote [] uncons drop]. \ only keep the first element

\\ non-destructive variants
[length& [] dup length].
