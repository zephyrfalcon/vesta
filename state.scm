;; state.scm

(use srfi-1)

(define *vesta-debug* #f)

;; interpreter state
(define-record state
  namespace-stack
  code-stack
  data-stack
  )

(define (make-initial-state code)
  (let ((toplevel-ns (create-namespace *builtin-namespace*)))
    (make-state (list toplevel-ns) code '())))

(define (pop-code! state)
  (let ((elem (first (state-code-stack state))))
    (state-code-stack-set! state (cdr (state-code-stack state)))
    elem))

(define (push-code! state code)
  ;; note: code is a list of words
  (state-code-stack-set! state (append code (state-code-stack state))))

(define (push-data! state value)
  (state-data-stack-set! state (cons value (state-data-stack state))))

(define (pop-data! state)
  (let ((value (first (state-data-stack state))))
    (state-data-stack-set! state (cdr (state-data-stack state)))
    value))

;; TODO: use SPLIT-AT (srfi-1)
(define (pop-many! state n)
  ;; pop N elements off the data stack, and return them as values.
  (let* ((ds (state-data-stack state))
         (vals (take ds n))
         (new-ds (drop ds n)))
    (state-data-stack-set! state new-ds)
    (apply values (reverse vals))))

(define (push-many! state . vals)
  (for-each (lambda (x) (push-data! state x)) vals))

(define (current-namespace state)
  (car (state-namespace-stack state)))

(define (push-namespace! state ns)
  (state-namespace-stack-set! state (cons ns (state-namespace-stack state))))

(define (pop-namespace! state)
  (let ((ns (first (state-namespace-stack state))))
    (state-namespace-stack-set! state (cdr (state-namespace-stack state)))
    ns))

;; execute the next word on the code stack, assuming there is one.
(define (execute-next state)
  (when *vesta-debug*
    (printf "CS: ~a~%" (state-code-stack state))
    (printf "NS: ~a~%" (state-namespace-stack state))
    (printf "DS: ~a~%" (state-data-stack state)))
  (let ((next-elem (pop-code! state)))
    (when *vesta-debug*
      (printf "Executing: ~a~%" next-elem)) 
    (if (or (list? next-elem)
            (number? next-elem))
        (push-data! state next-elem)
        (execute-word state next-elem))))

(define (execute-code state)
  ;; call EXECUTE-NEXT until the code stack is empty.
  (if (null? (state-code-stack state))
      'done
      (begin
        (execute-next state)
        (execute-code state))))

(define (execute-word state word)
  (let* ((ns (current-namespace state))
         (worddef (namespace-get-value ns word)))
    (cond
     ((not worddef)
      (error 'execute-word (sprintf "Undefined word: ~a~%" word)))
     ((procedure? worddef)
      (worddef state))
     ((list? worddef)
      (execute-user-word state worddef ns))
     (else (error 'execute-word "Invalid word definition")))))

;; does not actually execute the word directly, but creates a new namespace,
;; binds any arguments to names, and sets up the code stack so the word's body
;; will be executed next
(define (execute-user-word state worddef ns)
  (let* ((argnames (car worddef))
         (body (cdr worddef))
         (new-ns (create-namespace ns)))
    (bind-arguments state (reverse argnames) new-ns)
    (push-code! state (list '|[popns]|))
    (push-code! state body)
    (push-namespace! state new-ns)))

(define (bind-arguments state names ns)
  ;; binds names to values popped off the stack. note that the names must be
  ;; specified in reverse order: (c b a) will first bind c, then b, then a.
  (if (null? names)
      #t ;; done
      (let ((arg (car names))
            (value (pop-data! state)))
        ;; bind a small built-in function that pushes the value
        (namespace-define! ns arg (lambda (st) (push-data! st value)))
        (bind-arguments state (cdr names) ns))))

(define (execute-block state code)
  (let* ((body (force-list code))
         (ns (current-namespace state))
         (new-ns (create-namespace ns)))
    (push-code! state (list '|[popns]|))
    (push-code! state body)
    (push-namespace! state new-ns)))
