;; test-tokenizer.scm

(use test)
(load "tokenizer")

(test-group
 "find-match"

 (test '(whitespace "  ")    (find-match "  hello"))
 (test '(whitespace "\t")    (find-match "\ttoo bad"))
 (test '(symbol "+")         (find-match "+ x y"))
 (test '(symbol "ugo+rwx")   (find-match "ugo+rwx -"))
 (test '(number "33")        (find-match "33"))
 (test '(number "-7")        (find-match "-7 abs"))
 (test '(lbrace "[")         (find-match "[pop]"))
 (test '(rbrace "]")         (find-match "]."))
 (test '(comment "\\ bah\n") (find-match "\\ bah\nga toch weg"))
 )

(test-group
 "tokenize"

 (test '((number "1") (number "2") (number "3"))
       (tokenize "1 2 3"))
 (test '((symbol "+") (number "3") (symbol "-"))
       (tokenize "+ \\add\n3 -"))
 (test '((lbrace "[") (symbol "foo") (lbrace "[") (symbol "a") (rbrace "]")
         (symbol "bar") (symbol "baz") (rbrace "]") (symbol "."))
       (tokenize "[foo [a] bar baz]."))
 (test '((number "1") (number "2") (number "3"))
       (tokenize "\\ bogus\n1\n2\n3"))

 )
